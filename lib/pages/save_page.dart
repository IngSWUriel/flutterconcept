import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hello_world/db/operations.dart';
import '../models/notas.dart';
import 'package:hello_world/pages/save_foto.dart';

class SavePage extends StatelessWidget {
  static const String ROUTE = "/save";
  final _formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final titleController = TextEditingController();
  String imageUrl = '';
  @override
  Widget build(BuildContext context) {
    Note note = ModalRoute.of(context)?.settings.arguments as Note;
    _init(note);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        title: Text("Test Firestore Online"),
      ),
      body: Container(
          padding: EdgeInsets.all(20),
          child: Column(children: [
            _buildForm(note),
            ElevatedButton(
              child: Text("Upload Image",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20)),
              onPressed: () {
                Navigator.pushNamed(context, ImageUploads.ROUTE);
              },
            ),
          ])),
    );
  }

  _init(Note note) {
    titleController.text = note.title;
    nameController.text = note.name;
  }

  Widget _buildForm(Note note) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: nameController,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Complete data";
                }
                return null;
              },
              decoration: InputDecoration(
                  labelText: "Name",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(.5)))),
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              controller: titleController,
              maxLines: 8,
              maxLength: 1000,
              validator: (value) {
                if (value!.isEmpty) {
                  return "Complete data";
                }
                return null;
              },
              decoration: InputDecoration(
                  labelText: "Titulo",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(.5)))),
            ),
            ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    if (note.id != null) {
                      note.title = titleController.text;
                      note.name = nameController.text;
                      print(note);
                      FirebaseFirestore.instance
                          .collection('notas')
                          .doc(note.id)
                          .update({
                        'name': nameController.text,
                        'title': titleController.text
                      });
                      //Operation().update(note);
                    } else {
                      FirebaseFirestore.instance.collection('notas').add({
                        'name': nameController.text,
                        'title': titleController.text
                      });
                      /* Operation().insert(Note(
                          name: nameController.text,
                          title: titleController.text)); */
                    }
                  }
                },
                child: Text("Guardar"))
          ],
        ));
  }
}
