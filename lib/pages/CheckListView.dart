import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hello_world/models/notas.dart';
import 'package:hello_world/pages/save_page.dart';

import '../db/operations.dart';

class ListCheckList extends StatelessWidget {
  static const String ROUTE = "/";
  @override
  Widget build(BuildContext context) {
    return _MyList();
  }
}

class _MyList extends StatefulWidget {
  @override
  __MyListState createState() => __MyListState();
}

class __MyListState extends State<_MyList> {
  List<Note> notes = [];
  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.pushNamed(context, SavePage.ROUTE,
                    arguments: Note.empty())
                .then((value) => setState(() {
                      _loadData();
                    }));
          },
        ),
        appBar: AppBar(title: Text('Prueba de Concepto')),
        body: StreamBuilder(
          stream: FirebaseFirestore.instance.collection('notas').snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            List<DocumentSnapshot> docs = snapshot.data!.docs;
            return Container(
              child: ListView.builder(
                itemCount: docs.length,
                itemBuilder: (_, i) {
                  Map<String, dynamic>? data =
                      docs[i].data() as Map<String, dynamic>?;
                  return _createItem(docs[i].id, data!['name'], data!['title']);
                },
              ),
            );
          },
        ));
  }

  _loadData() async {
    List<Note> auxNote = await Operation().notes();
    setState(() {
      notes = auxNote;
    });
  }

  _createItem(String i, String name, String title) {
    return Dismissible(
      key: Key(i.toString()),
      direction: DismissDirection.startToEnd,
      background: Container(
        color: Colors.red,
        padding: EdgeInsets.only(left: 5),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        ),
      ),
      onDismissed: (direction) {
        FirebaseFirestore.instance.collection('notas').doc(i).delete();
      },
      child: ListTile(
        title: Text(title),
        trailing: MaterialButton(
            onPressed: () {
              Navigator.pushNamed(context, SavePage.ROUTE,
                      arguments: Note(id: i, name: name, title: title))
                  .then((value) => setState(() {
                        _loadData();
                      }));
            },
            child: Icon(Icons.edit)),
      ),
    );
  }
}
