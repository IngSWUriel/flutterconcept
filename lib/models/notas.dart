class Note {
  String? id;
  String name = '';
  String title = '';

  Note({this.id, required this.name, required this.title});
  Note.empty();
  Map<String, dynamic> toMap() {
    return {'id': id, 'name': name, 'title': title};
  }
}
