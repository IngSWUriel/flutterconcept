import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../models/notas.dart';

class Operation {
  Future<Database> _openDB() async {
    return openDatabase(join(await getDatabasesPath(), "notes.db"),
        onCreate: (db, version) {
      return db.execute(
          "CREATE TABLE notes (id INTEGER PRIMARY KEY ,name TEXT, title TEXT)");
    }, version: 1);
  }

  Future<int> insert(Note note) async {
    Database database = await _openDB();
    return database.insert("notes", note.toMap());
  }

  Future<int> delete(String note) async {
    Database database = await _openDB();
    return database.delete("notes", where: 'id = ?', whereArgs: [note]);
  }

  Future<int> update(Note note) async {
    Database database = await _openDB();
    print('++++++' + note.id!);
    return database
        .update("notes", note.toMap(), where: 'uid = ?', whereArgs: [note.id]);
  }

  Future<List<Note>> notes() async {
    Database database = await _openDB();
    final List<Map<String, dynamic>> notesMap = await database.query("notes");
    return List.generate(
        notesMap.length,
        (index) => Note(
            id: notesMap[index]['id'].toString(),
            name: notesMap[index]['name'],
            title: notesMap[index]['title']));
  }
}
