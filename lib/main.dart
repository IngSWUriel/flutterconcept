import 'package:flutter/material.dart';
import 'package:hello_world/pages/save_foto.dart';
import 'pages/CheckListView.dart';
import 'pages/save_page.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        initialRoute: ListCheckList.ROUTE,
        routes: {
          ListCheckList.ROUTE: (_) => ListCheckList(),
          SavePage.ROUTE: (_) => SavePage(),
          ImageUploads.ROUTE: (_) => ImageUploads()
        },
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primaryColor: Color(0xFF5F5FFF),
            accentColor: Color(0XFF030047),
            highlightColor: Color(0XFFB7B7D2),
            textTheme: TextTheme(
              headline1: TextStyle(
                fontSize: 36.0,
                fontWeight: FontWeight.bold,
                color: Color(0XFF030047),
              ),
              headline2: TextStyle(
                fontSize: 36.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFF5F5FFF),
              ),
              headline3: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
              headline4: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
                color: Color(0XFF030047),
              ),
              bodyText1: TextStyle(
                fontSize: 20.0,
                color: Color(0XFFB7B7D2),
              ),
              bodyText2: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
                color: Color(0xFF5F5FFF),
              ),
              subtitle1: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w600,
                color: Color(0XFFB7B7D2),
              ),
            )));
  }
}
